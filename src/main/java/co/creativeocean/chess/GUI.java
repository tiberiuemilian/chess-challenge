package co.creativeocean.chess;

import co.creativeocean.chess.game.Game;
import co.creativeocean.chess.game.Spot;
import co.creativeocean.chess.pieces.Piece;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.net.URL;

@SpringBootApplication
public class GUI extends JFrame implements MouseListener, MouseMotionListener, CommandLineRunner {

    private static Logger LOG = LoggerFactory.getLogger(GUI.class);

    private static final String resourcesPath = "img";
    private JLayeredPane layeredPane;
    private JPanel chessBoard;
    private Game game = new Game();
    private int startX;
    private int startY;
    private JLabel chessPiece;
    private int xAdjustment;
    private int yAdjustment;

    public GUI  () {
        Dimension boardSize = new Dimension(600, 600);

        //  Use a Layered Pane for this application
        layeredPane = new JLayeredPane();
        getContentPane().add(layeredPane);
        layeredPane.setPreferredSize(boardSize);
        layeredPane.addMouseListener(this);
        layeredPane.addMouseMotionListener(this);

        //Add a chess board to the Layered Pane
        chessBoard = new JPanel();
        layeredPane.add(chessBoard, JLayeredPane.DEFAULT_LAYER);
        chessBoard.setLayout(new GridLayout(8, 8));
        chessBoard.setPreferredSize(boardSize);
        chessBoard.setBounds(0, 0, boardSize.width, boardSize.height);

        for (int i = 0; i < 64; i++) {
            JPanel square = new JPanel(new BorderLayout());
            chessBoard.add(square);

            int row = (i / 8) % 2;
            if (row == 0)
                square.setBackground(i % 2 == 0 ? Color.gray : Color.white);
            else
                square.setBackground(i % 2 == 0 ? Color.white : Color.gray);
        }

        for (int i=0; i<8; i++) {
            for (int j=0; j<8; j++) {
                Piece piece = game.board().boxes()[i][j].piece();
                if (piece != null) {
                    String type = piece.getClass().getSimpleName();
                    String pieceImgPath = resourcesPath
                                                .concat(File.separator)
                                                .concat(StringUtils.capitalize(piece.color().name().toLowerCase()))
                                                .concat(type)
                                                .concat(".png");
                    JLabel pieceIcon = new JLabel(createImageIcon(pieceImgPath, type));
                    JPanel panel = (JPanel) chessBoard.getComponent(j*8 + i);
                    panel.add(pieceIcon);

                }
            }
        }
    }

    private ImageIcon createImageIcon(String path, String description) {
        URL imgURL = getClass().getClassLoader().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    public static void main(String[] args) {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(GUI.class, args);
    }

    @Override
    public void run(String... args) {
        LOG.info("EXECUTING : command line runner");
        for (int i = 0; i < args.length; ++i) {
            LOG.info("args[{}]: {}", i, args[i]);
        }

        GUI chessProject = new GUI();
        chessProject.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        chessProject.pack();
        chessProject.setResizable(true);
        chessProject.setLocationRelativeTo(null);
        chessProject.setVisible(true);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        startX = (e.getX() / 75);
        startY = (e.getY() / 75);
        if (game.board().boxes()[startX][startY].piece() != null) {
            Component component = chessBoard.findComponentAt(e.getX(), e.getY());
            if (component instanceof JLabel) {
                chessPiece = (JLabel) component;
                Point parentLocation = component.getParent().getLocation();
                xAdjustment = parentLocation.x - e.getX();
                yAdjustment = parentLocation.y - e.getY();
                chessPiece.setLocation(e.getX() + xAdjustment, e.getY() + yAdjustment);
                chessPiece.setSize(chessPiece.getWidth(), chessPiece.getHeight());
                layeredPane.add(chessPiece, JLayeredPane.DRAG_LAYER);
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (chessPiece == null) return;
        chessPiece.setLocation(e.getX() + xAdjustment, e.getY() + yAdjustment);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (chessPiece == null) {
            return;
        }

        Piece piece = game.board().boxes()[startX][startY].piece();
        Spot start = game.board().boxes()[startX][startY];
        chessPiece.setVisible(false);

        // avoid moves outside of the chess table
        if ((e.getX() < 0) || (e.getY() < 0) || (e.getX() > 75 * 8) || (e.getY() > 75 * 8)) {
            undo(piece);
            chessPiece = null;
            return;
        }


        int endX = (e.getX() / 75);
        int endY = (e.getY() / 75);

        Spot end = game.board().boxes()[endX][endY];
        if (!piece.isValidMove(game.board(), start, end)) {
            // revert on GUI
            undo(piece);
        } else {
            // try to move on model
            game.board().move(start, end);
            if (game.board().isInCheck(co.creativeocean.chess.game.Color.opponent(game.board().getTurn()))) {
                // undo
                game.board().undoLastMove();
                undo(piece);
            } else {
                // move on GUI
                move(e);
            }
        }

        chessPiece = null;
    }

    private void move(MouseEvent e) {
        Component c = chessBoard.findComponentAt(e.getX(), e.getY());
        if (c instanceof JLabel) {
            Container parent = c.getParent();
            parent.remove(0);
            parent.add(chessPiece);
        } else {
            Container parent = (Container) c;
            parent.add(chessPiece);
        }
        chessPiece.setVisible(true);
    }

    private void undo(Piece piece) {
        String type = piece.getClass().getSimpleName();
        String pieceImgPath = resourcesPath
                .concat(File.separator)
                .concat(StringUtils.capitalize(piece.color().name().toLowerCase()))
                .concat(type)
                .concat(".png");
        JLabel pieceIcon = new JLabel(createImageIcon(pieceImgPath, type));
        JPanel panel = (JPanel) chessBoard.getComponent((startY * 8) + startX);
        panel.add(pieceIcon);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
