package co.creativeocean.chess.game;

import co.creativeocean.chess.pieces.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

public class Board {

    @Accessors(fluent = true) @Getter
    private Spot[][] boxes = new Spot[8][8];

    @Getter @Setter
    private Color turn = Color.WHITE;

    private Spot lastMoveStart = null;

    private Spot lastMoveEnd = null;

    public Board() {
        resetBoard();
    }

    public void resetBoard()  {
        // initialize white pieces
        boxes[0][0] = new Spot(0, 0, new Rook(Color.WHITE));
        boxes[0][1] = new Spot(0, 1, new Knight(Color.WHITE));
        boxes[0][2] = new Spot(0, 2, new Bishop(Color.WHITE));
        boxes[0][3] = new Spot(0, 3, new Queen(Color.WHITE));
        boxes[0][4] = new Spot(0, 4, new King(Color.WHITE));
        boxes[0][5] = new Spot(0, 5, new Bishop(Color.WHITE));
        boxes[0][6] = new Spot(0, 6, new Knight(Color.WHITE));
        boxes[0][7] = new Spot(0, 7, new Rook(Color.WHITE));
        // pawns ...
        for (int i=0; i<8; i++) {
            boxes[1][i] = new Spot(1, i, new Pawn(Color.WHITE));
        }

        // initialize black pieces
        boxes[7][0] = new Spot(7, 0, new Rook(Color.BLACK));
        boxes[7][1] = new Spot(7, 1, new Knight(Color.BLACK));
        boxes[7][2] = new Spot(7, 2, new Bishop(Color.BLACK));
        boxes[7][3] = new Spot(7, 3, new Queen(Color.BLACK));
        boxes[7][4] = new Spot(7, 4, new King(Color.BLACK));
        boxes[7][5] = new Spot(7, 5, new Bishop(Color.BLACK));
        boxes[7][6] = new Spot(7, 6, new Knight(Color.BLACK));
        boxes[7][7] = new Spot(7, 7, new Rook(Color.BLACK));
        // pawns ...
        for (int i=0; i<8; i++) {
            boxes[6][i] = new Spot(6, i, new Pawn(Color.BLACK));
        }

        // initialize remaining boxes without any piece
        for (int i = 2; i < 6; i++) {
            for (int j = 0; j < 8; j++) {
                boxes[i][j] = new Spot(i, j, null);
            }
        }
    }

    public void move(Spot start, Spot end) {
        // move piece on the model
        lastMoveStart = start.clone();
        lastMoveEnd = end.clone();

        Piece piece = start.piece();
        start.piece(null);
        end.piece(piece);

        switchTurn();
    }

    public void undoLastMove() {
        boxes[lastMoveStart.x()][lastMoveStart.y()] = lastMoveStart;
        boxes[lastMoveEnd.x()][lastMoveEnd.y()] = lastMoveEnd;

        switchTurn();
    }

    /**
     * turn for next player
     */
    public void switchTurn() {
        turn = Color.opponent(turn);
    }

    public boolean isInCheck(Color color) {
        Spot currentSpot = null;
        Piece currentPiece = null;
        Spot kingPosition = null;
        for (int i=0; i<8; i++) {
            for (int j=0; j<8; j++) {
                currentSpot = boxes[i][j];
                currentPiece = currentSpot.piece();
                if ((currentPiece != null) && currentPiece.isKing(color)) {
                    kingPosition = currentSpot;
                    break;
                }
            }
        }

        for (int i=0; i<8; i++) {
            for (int j=0; j<8; j++) {
                currentSpot = boxes[i][j];
                currentPiece = currentSpot.piece();

                if ((currentPiece != null) && (! currentPiece.color().equals(color)) // opponent piece
                        && (! currentPiece.getClass().getSimpleName().equals("Pawn"))) { // TODO: remove this part of the condition after uncommented Pawn functionality
                    if (currentPiece.isValidMove(this, currentSpot, kingPosition)) {
                        return true;
                    }
                }
            }
        }
        // no threat
        return false;
    }

}
