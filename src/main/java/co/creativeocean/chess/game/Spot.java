package co.creativeocean.chess.game;

import co.creativeocean.chess.pieces.Piece;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Getter @Setter @AllArgsConstructor
public class Spot implements Cloneable {

    private int x;
    private int y;
    private Piece piece;

    public boolean isAPieceWithSameColorOnIt(Piece piece) {
        if ((piece != null) && (this.piece != null) && (piece.color().equals(this.piece.color()))) {
            return true;
        }
        return false;
    }

    @Override
    protected Spot clone() {
        Spot clone = new Spot(this.x, this.y, this.piece);
        return clone;
    }
}
