package co.creativeocean.chess.game;

import lombok.Getter;
import lombok.experimental.Accessors;

public class Game {

    @Accessors(fluent = true)
    @Getter
    private Board board = new Board();
}
