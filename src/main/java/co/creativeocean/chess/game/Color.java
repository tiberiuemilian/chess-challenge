package co.creativeocean.chess.game;

public enum Color {
    WHITE, BLACK;

    public static Color opponent(Color color) {
        return color.equals(Color.WHITE) ? Color.BLACK : Color.WHITE;
    }
}
