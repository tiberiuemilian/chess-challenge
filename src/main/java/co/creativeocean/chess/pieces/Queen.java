package co.creativeocean.chess.pieces;

import co.creativeocean.chess.game.Board;
import co.creativeocean.chess.game.Color;
import co.creativeocean.chess.game.Spot;

public class Queen extends Piece {

    public Queen(Color color) {
        super(color);
    }

    @Override
    public boolean isValidMove(Board board, Spot start, Spot end) {
        if (super.isValidMove(board, start, end)) {
            if (Rook.isValidRookMove(board, start, end, log)) {
                return true;
            }

            if (Bishop.isValidBishopMove(board, start, end, log)) {
                return true;
            }
        }
        return false;
    }
}
