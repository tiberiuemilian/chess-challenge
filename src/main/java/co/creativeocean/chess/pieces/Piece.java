package co.creativeocean.chess.pieces;

import co.creativeocean.chess.game.Board;
import co.creativeocean.chess.game.Color;
import co.creativeocean.chess.game.Spot;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RequiredArgsConstructor
@Getter @Setter
public abstract class Piece {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    private boolean killed = false;

    private boolean firstMove = true;

    @Accessors(fluent = true)
    private final Color color;

    public boolean isValidMove(Board board, Spot start, Spot end) {
        Piece piece = start.piece();
        return (isYourTurn(board, piece)) &&
                (! end.isAPieceWithSameColorOnIt(piece));
    }

    protected boolean isYourTurn(Board board, Piece piece) {
        return board.getTurn().equals(piece.color());
    }

    public boolean isKing(Color color) {
        return ((this.color == color)
                    && getClass().getSimpleName().equals("King"));
    }
}
