package co.creativeocean.chess.pieces;

import co.creativeocean.chess.game.Board;
import co.creativeocean.chess.game.Color;
import co.creativeocean.chess.game.Spot;

public class Pawn extends Piece {
    public Pawn(Color color) {
        super(color);
    }

    @Override
    public boolean isValidMove(Board board, Spot start, Spot end) {
        if (super.isValidMove(board, start, end)) {
//            int xMovement = end.x() - start.x();
//            int yMovement = end.y() - start.y();
//            if (start.piece().color().equals(Color.WHITE)) { // white Pawn
//                if (start.y() == end.y()) {
//                    if (xMovement == 1) {
//                        log.debug("one step on y: {},{} -> {},{}", start.x(), start.y(), end.x(), end.y());
//                        return true;
//                    }
//
//                    if ((start.x() == 6) && // white Pawn first 2 steps move
//                            (xMovement == -2) &&
//                            (board.boxes()[start.x()][start.y() - 1].piece() == null)) {
//                        return true;
//                    }
//                }
//
//                if ((yMovement == 1) && (Math.abs(xMovement) == 1) &&
//                        (board.boxes()[end.x()][end.y()].piece() != null)) {
//                    return true;
//                }
//            }
//
//
//            if (start.y() == end.y()) {
//                if ((start.piece().color().equals(Color.WHITE) && (xMovement == 1)) ||
//                        (start.piece().color().equals(Color.BLACK) && (xMovement == -1))) {
//                    log.debug("one step on y: {},{} -> {},{}", start.x(), start.y(), end.x(), end.y());
//                    return true;
//                }
//
//                // first move ; 2 steps
//                if ((start.piece().color().equals(Color.WHITE) && (start.x() == 6)) &&
//                        (xMovement == -2) &&
//                        (board.boxes()[start.x()][start.y() - 1].piece() != null)) {
//                    return true;
//                }
//                if ((start.piece().color().equals(Color.BLACK) && (start.x() == 1)) &&
//                        (xMovement == 2) &&
//                        (board.boxes()[start.x()][start.y() + 1].piece() != null)) {
//                    return true;
//                }
//            }
//
////            if () {
////
////            }

            return true;
        }
        return false;
    }

}
