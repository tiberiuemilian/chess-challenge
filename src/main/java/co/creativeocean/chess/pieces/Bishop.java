package co.creativeocean.chess.pieces;

import co.creativeocean.chess.game.Board;
import co.creativeocean.chess.game.Color;
import co.creativeocean.chess.game.Spot;
import org.slf4j.Logger;

public class Bishop extends Piece {

    public Bishop(Color color) {
        super(color);
    }

    @Override
    public boolean isValidMove(Board board, Spot start, Spot end) {
        if (super.isValidMove(board, start, end)) {
            if (isValidBishopMove(board, start, end, log)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidBishopMove(Board board, Spot start, Spot end, Logger log) {
        if (Math.abs(end.x() - start.x()) == Math.abs(end.y() - start.y())) {
            log.debug("diagonal move: {},{} -> {},{}", start.x(), start.y(), end.x(), end.y());
            int coordDiff = end.x() - start.x();
            int ycoeff = 0;
            int xcoeff = 0;
            if(end.y() - start.y() > 0){ //determine whether the algorithm needs to go up or down on y axis
                ycoeff = 1;
            }else{
                ycoeff = -1;
            }
            if(end.x() - start.x() > 0){ //determine whether the algorithm needs to go left or right on x axis
                xcoeff = 1;
            }else{
                xcoeff = -1;
            }

            for (int i = 1; i <= Math.abs(coordDiff) - 1; i++) { //checks for pieces in the way of bishop path
                if (board.boxes()[start.x() + i*xcoeff][start.y() + i*ycoeff].piece() != null) {
                    return false;
                }
            }

            return true;
        }
        return false;
    }

}
