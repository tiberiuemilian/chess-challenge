package co.creativeocean.chess.pieces;

import co.creativeocean.chess.game.Board;
import co.creativeocean.chess.game.Color;
import co.creativeocean.chess.game.Spot;

public class King extends Piece {
    public King(Color color) {
        super(color);
    }

    @Override
    public boolean isValidMove(Board board, Spot start, Spot end) {
        if (super.isValidMove(board, start, end)) {
            int xMovement = Math.abs(end.x() - start.x());
            int yMovement = Math.abs(end.y() - start.y());
            if ((xMovement <= 1) && (yMovement <=1)) {
                return true;
            }
        }
        return false;
    }
}

