package co.creativeocean.chess.pieces;

import co.creativeocean.chess.game.Board;
import co.creativeocean.chess.game.Color;
import co.creativeocean.chess.game.Spot;

public class Knight extends Piece {

    public Knight(Color color) {
        super(color);
    }

    @Override
    public boolean isValidMove(Board board, Spot start, Spot end) {
        if (super.isValidMove(board, start, end)) {
            if (((Math.abs(end.x() - start.x()) == 1) && (Math.abs(end.y() - start.y()) == 2)) ||
                    ((Math.abs(end.y() - start.y()) == 1) && (Math.abs(end.x() - start.x()) == 2))) {
                log.debug("valid knight move: {},{} -> {},{}", start.x(), start.y(), end.x(), end.y());
                return true;
            }
        }
        return false;
    }

}
