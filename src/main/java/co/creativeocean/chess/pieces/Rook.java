package co.creativeocean.chess.pieces;

import co.creativeocean.chess.game.Board;
import co.creativeocean.chess.game.Color;
import co.creativeocean.chess.game.Spot;
import org.slf4j.Logger;

public class Rook extends Piece {

    public Rook(Color color) {
        super(color);
    }

    @Override
    public boolean isValidMove(Board board, Spot start, Spot end) {
        if (super.isValidMove(board, start, end)) {
            if (isValidRookMove(board, start, end, log)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isValidRookMove(Board board, Spot start, Spot end, Logger log) {
        if (isValidMoveOnX(board, start, end, log)) {
            return true;
        }

        if (isValidMoveOnY(board, start, end, log)) {
            return true;
        }

        return false;
    }

    private static boolean isValidMoveOnY(Board board, Spot start, Spot end, Logger log) {
        if (start.y() == end.y()) {
            log.debug("y axis is same");
            int xdir = Math.round(Math.signum(end.x() - start.x()));
            for (int i = start.x() + xdir; i != end.x(); i += xdir) {
                log.debug("checking position: {},{}", i, start.y());
                if (board.boxes()[i][start.y()].piece() != null) {
                    log.debug("blocked path: {},{}", i, start.y());
                    return false;
                }
            }

            return true;
        }
        return false;
    }

    private static boolean isValidMoveOnX(Board board, Spot start, Spot end, Logger log) {
        if (start.x() == end.x()) {
            log.debug("x axis is the same");
            int ydir = Math.round(Math.signum(end.y() - start.y()));
            for (int i = start.y() + ydir; i != end.y(); i += ydir) {
                log.debug("checking position: {},{}", start.x(), i);
                if (board.boxes()[start.x()][i].piece() != null) {
                    log.debug("blocked path: {},{}", start.x(), i);
                    return false;
                }
            }

            return true;
        }
        return false;
    }

}
